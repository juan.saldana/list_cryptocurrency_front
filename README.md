# ListCryptocurrencyFront

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.4.

## To run this front application you need have instaled before:
    - Node JS
    - Angular 2+

## First
Run `npm i` to install all dependencies of project

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
