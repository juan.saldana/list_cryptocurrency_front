export const environment = {
  urlApi: 'http://3.230.76.19:8080/api',
  urlCrypto: 'https://api.coingecko.com/api/v3/coins/list?include_platform=true',
  urlIp: 'https://ipinfo.io?token=4185c58bbf1d80',
  production: true
};
