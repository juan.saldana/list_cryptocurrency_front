export interface Client {
    id: string;
    latitude: string;
    longitude: string;
    ip: string;
    device: string;
    search_date: Date;
}