export interface Coin {
    id: string;
    name: string;
    symbol: string;
    platforms: object;
    keys: Array<string>;
}