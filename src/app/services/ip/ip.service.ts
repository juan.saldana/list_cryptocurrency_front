import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ip } from 'src/app/models/ip';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IpService {

  constructor(private http: HttpClient) { }

  public getIp(): Observable<Ip> {
    return this.http.get<Ip>(environment.urlIp);
  }
}
