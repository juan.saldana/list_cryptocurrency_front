import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Client } from '../../models/client';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  public getClients(): Observable<Client[]> {
    return this.http.get<Client[]>(`${environment.urlApi}/clients`);
  }

  public addClients(client: Client): Observable<Client> {
    return this.http.post<Client>(`${environment.urlApi}/clients`, client);
  }
}
