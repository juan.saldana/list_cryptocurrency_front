import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Coin } from 'src/app/models/coin';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  constructor(private http: HttpClient) { }

  public getCryptos(): Observable<Coin[]> {
    return this.http.get<Coin[]>(environment.urlCrypto);
  }

}
