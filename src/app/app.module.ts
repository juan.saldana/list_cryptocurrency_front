import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ClientComponent } from './components/client.component';
import { HttpClientModule } from '@angular/common/http';
import { ClientService } from './services/client/client.service';
import { CryptoService } from './services/crypto/crypto.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { IpService } from './services/ip/ip.service';

@NgModule({
  declarations: [
    AppComponent,
    ClientComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule
  ],
  providers: [ClientService, CryptoService, IpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
