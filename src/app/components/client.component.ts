import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Client } from '../models/client';
import { Coin } from '../models/coin';
import { ClientService } from '../services/client/client.service';
import { CryptoService } from '../services/crypto/crypto.service';
import { IpService } from '../services/ip/ip.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'name',
    'symbol',
    'platforms'
  ];
  client: Client = {
    id: null,
    latitude: 'Null data',
    longitude: 'Null data',
    ip: 'Null data',
    device: window.screen.width <= 768 ? 'MOBILE' : 'DESKTOP',
    search_date: new Date()
  };
  dataSource: MatTableDataSource<Coin>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private cryptoService: CryptoService,
    private clientService: ClientService,
    private ipService: IpService
  ) { }

  ngOnInit(): void {
    this.uploadCryptos();
    this.chargeClient();
  }

  chargeClient(){
    this.ipService.getIp().subscribe(data => {
      if (!data) return;

      const loc = data.loc.split(',');
      this.client.latitude = loc[0];
      this.client.longitude = loc[1];
      this.client.ip = data.ip;
    });
  }

  splitKeys(key: string){
    const splited = key.split('-');
    let finalKey = '';
    splited.forEach(word => {
      finalKey += word.slice(0,1);
    });
    return finalKey;
  }

  uploadCryptos() {
    this.cryptoService.getCryptos().subscribe(data => {
      if (!data){
        this.dataSource = new MatTableDataSource([]);
      }else {
        data.forEach(coin => {
          let finalKeys = [];
          const keys = Object.keys(coin.platforms);
          keys.forEach(key => {
            finalKeys.push(`${key.length > 12 ? this.splitKeys(key) : key} : ${coin.platforms[key] === '' ? 'Wallet empty' : coin.platforms[key]}`);
          });
          coin.keys = finalKeys;
        });
        this.dataSource = new MatTableDataSource(data);
      }

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  saveDate() {
    if (this.client.ip === 'Null data') {
      alert('No data to save');
    }else {
      this.clientService.addClients(this.client).subscribe(data => {
        if (!data){
          alert('Client already exist');
        }else{
          alert(`Client saved: ${data.id} with ip: ${data.ip}`);
        }
      });
    }
  }

}